proc second() =
  raise newException(IOError, "Oh noes!")

proc first() =
  try:
    second()
#  except IOError:
#    echo("Caught IOError: " & getCurrentExceptionMsg())
  except:
    echo("Caught exception: " & getCurrentExceptionMsg())
  finally:
    echo("Always runs")

first()

