import database
import os, times

when isMainModule:
  const fileName = "tweeter_test.db"
  removeFile(fileName)
  var db = newDatabase(fileName)

  db.init()
  db.create(User(username: "cjs496"))
  db.create(User(username: "d0m96"))
  db.create(User(username: "nim_lang"))

  db.post(Message(username: "nim_lang", time: getTime() - 4.seconds,
                  msg: "Hello Nim in Action readers!"))
  db.post(Message(username: "nim_lang", time: getTime(),
                  msg: "99.9% off Nim in Action for everyone, " &
                       "for the next minute only!"))

  var dom, nim, cjs: User;
  doAssert(db.findUser("d0m96", dom))
  doAssert(db.findUser("nim_lang", nim))
  doAssert(db.findUser("cjs496", cjs))
  doAssert(not db.findUser("", dom))
  doAssert(not db.findUser("xxx", nim))
  doAssert(not db.findUser("nil", cjs))

  # d0m96 follows nim_lang
  db.follow(dom, nim)
  doAssert(db.findUser("d0m96", dom))

  let messages = db.findMessages(dom.following)
  doAssert(messages[0].msg == "99.9% off Nim in Action for everyone, " &
                              "for the next minute only!")
  doAssert(messages[1].msg == "Hello Nim in Action readers!")
  echo "Tests finished successfully!"

