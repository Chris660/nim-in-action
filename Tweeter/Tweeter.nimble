# Package

version       = "0.1.0"
author        = "Chris Sykes"
description   = "A simple Twitter like web-app"
license       = "MIT"
srcDir        = "src"
bin           = @["tweeter"]
skipExt       = @["nim"]

# Dependencies

requires "nim >= 0.18.0", "jester >= 0.0.1"
