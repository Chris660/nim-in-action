import asyncdispatch, jester, times
import database, views/user, views/general

let db = newDatabase()

proc userLogin(db: Database, request: Request, user: var User): bool =
  if request.cookies.hasKey("username"):
    let name = request.cookies["username"]
    if not db.findUser(name, user):
      user = User(username: name, following: @[])
      db.create(user)
    return true
  else:
    return false

routes:
  get "/":
    var user: User
    if userLogin(db, request, user):
      let messages = db.findMessages(user.following & user.username)
      resp renderMain(renderTimeline(user.username, messages))
    else:
      resp renderMain(renderLogin())

  get "/@name":
    cond @"name" != "style.css"
    var user: User
    if not db.findUser(@"name", user):
      halt "User not found"
    let messages = db.findMessages(@[user.username])

    var currentUser: User
    if userLogin(db, request, currentUser):
      resp renderMain(renderUser(user, currentUser) & renderMessages(messages))
    else:
      resp renderMain(renderUser(currentUser) & renderMessages(messages))

  post "/login":
    setCookie("username", @"username", getTime().utc() + 2.hours)
    redirect("/")

  post "/createMessage":
    let message = Message(
      username: @"username",
      time: getTime(),
      msg: @"message")

    try:
      db.post(message)
      redirect("/")
    except ValueError:
      halt getCurrentExceptionMsg()

  post "/follow":
    var follower, target: User
    if not db.findUser(@"follower", follower):
      halt "Follower not found!"
    if not db.findUser(@"target", target):
      halt "Target not found!"
    db.follow(follower, target)
    redirect(uri("/" & @"target"))

runforever()

