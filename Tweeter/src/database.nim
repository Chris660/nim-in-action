import db_sqlite
import times
import strutils

type
  User* = object
    username*: string
    following*: seq[string]

  Message* = object
    username*: string
    time*: Time
    msg*: string

  Database* = ref object
    db: DbConn

const dbName* = "tweeter.db"

proc newDatabase*(filename = dbName): Database =
  new result # to avoid nil ref
  result.db = open(filename, "", "", "")


# Create primatives
proc post*(dbase: Database, message: Message) =
  if message.msg.len > 140:
    raise newException(ValueError, "Message too long.")

  dbase.db.exec(sql"INSERT INTO Message VALUES (?, ?, ?);",
    message.username, $message.time.toUnix().int, message.msg)

proc follow*(dbase: Database, follower, user: User) =
  dbase.db.exec(sql"INSERT INTO Following VALUES (?, ?);",
    follower.username, user.username)

proc create*(dbase: Database, user: User) =
  dbase.db.exec(sql"INSERT INTO User VALUES (?);", user.username)
  

# Read primatives
proc findUser*(dbase: Database, username: string, user: var User): bool =
  let row = dbase.db.getRow(
      sql"SELECT username FROM User WHERE username = ?;", username)
  if row[0].len == 0: return false
  else: user.username = row[0]

  let following = dbase.db.getAllRows(
      sql"SELECT followed_user FROM Following WHERE follower = ?;", username)
  user.following = @[]
  for row in following:
    if row[0].len != 0:
      user.following.add(row[0])

  return true


proc findMessages*(dbase: Database, usernames: seq[string],
                   limit = 10): seq[Message] =
  result = @[]
  if usernames.len == 0: return

  # Build up a dynamic WHERE clause
  var whereClause = " WHERE "
  for i in 0 ..< usernames.len:
    whereClause.add("username = ? ")
    if i != pred usernames.len:
      whereClause.add("or ")

  let messages = dbase.db.getAllRows(
      sql("SELECT username, time, msg FROM Message" &
          whereClause & "ORDER BY time DESC LIMIT " & $limit),
      usernames)
  for row in messages:
    result.add(Message(username: row[0],
                       time: fromUnix(row[1].parseInt),
                       msg: row[2]))


proc init*(dbase: Database) =
  dbase.db.exec(sql"""
    CREATE TABLE IF NOT EXISTS User(
      username text PRIMARY KEY
    );
    """)

  dbase.db.exec(sql"""
    CREATE TABLE IF NOT EXISTS Following(
      follower text,
      followed_user text,
      PRIMARY KEY (follower, followed_user),
      FOREIGN KEY (follower) REFERENCES User(username),
      FOREIGN KEY (followed_user) REFERENCES User(username)
    );
  """)

  dbase.db.exec(sql"""
    CREATE TABLE IF NOT EXISTS Message(
      username text,
      time text,
      msg text NOT NULL,
      FOREIGN KEY (username) REFERENCES User(username)
    );
  """)


proc close*(dbase: Database) =
  dbase.db.close()

