# Nim Chat protocol server.

import asyncdispatch, asyncnet, protocol

type
  Client = ref object
    socket: AsyncSocket
    netAddr: string
    id: int
    connected: bool

  Server = ref object
    socket: AsyncSocket
    clients: seq[Client]

proc `$`(client: Client) : string =
  $client.id & "(" & client.netAddr & ")"

proc newServer*() : Server =
  Server(socket: newAsyncSocket(), clients: @[])


proc processMessages(server: Server, client: Client) {.async.} =
  while true:
    let line = await client.socket.recvLine()
    if line.len == 0:
      echo(client, " disconnected")
      client.connected = false
      client.socket.close()
      return

    let msg = parseMessage(line)
    if msg.message == "bye":
      echo(client, " quit the server :-(")
      client.connected = false
      client.socket.close()
      return

    if msg.message != "\r\n":
      var sent = false
      for c in server.clients:
        if c.id != client.id and c.connected:
          await c.socket.send(line & "\r\n")
          sent = true


proc loop(server: Server, port = 7687) {.async.} =
  server.socket.bindAddr(port.Port)
  server.socket.listen()

  while true:
    let (address , clientSocket) = await server.socket.acceptAddr()
    echo "Accepted client from " & address
    let client = Client(
      socket: clientSocket,
      netAddr: address,
      id: server.clients.len,
      connected: true)
    server.clients.add(client)
    asyncCheck processMessages(server, client)


var server = newServer()
waitFor loop(server)

