# Nim chat client

from os import nil
import threadpool
import protocol
import asyncdispatch, asyncnet

echo "Chat client started."

proc connect(socket: AsyncSocket, serverAddr: string) {.async.} =
  echo("Connecting to ", serverAddr)
  await socket.connect(serverAddr, defaultPort.Port)
  echo("Connected!")
  while true:
    let line = await socket.recvLine()
    if line.len > 0:
      let parsed = parseMessage(line)
      echo(parsed.user, " said ", parsed.message)
    else:
      echo("Disconnected!")
      break


if os.paramCount() == 0:
  quit "usage: client SERVER_HOSTNAME"

let serverAddr = os.paramStr(1)
var socket = newAsyncSocket()
asyncCheck connect(socket, serverAddr)

var messageFlowVar = spawn stdin.readLine()
while true:
  if messageFlowVar.isReady():
    let message = createMessage("Anonymous", ^messageFlowVar)
    echo("sending: ", message)
    asyncCheck socket.send(message)
    messageFlowVar = spawn stdin.readLine()
  asyncdispatch.poll()

