# Chat server protocol

# Messages are JSON objects
import json

type
  Message* = object
    user*: string
    message*: string

const defaultPort* = 7687
const msgDelimiter = "\r\n"

proc parseMessage*(message: string): Message =
  #[
    {
      "username": "Joe",
      "message": "Hi there!"
    }
  ]#
  let jsonMsg = parseJson(message)
  result.user = jsonMsg["username"].getStr()
  result.message = jsonMsg["message"].getStr()


proc createMessage*(user, message: string) : string =
  result = $(%{
    "username": %user,
    "message": %message
  }) & msgDelimiter

proc createMessage*(message: Message) : string =
  createMessage(message.user, message.message)


when isMainModule:
  block:
    let data = """{ "username": "Chris.Sykes", "message": "Hello, Sir!" }"""
    let parsed = parseMessage(data)
    doAssert parsed.user == "Chris.Sykes"
    doAssert parsed.message == "Hello, Sir!"

  block:
    let data = "[ 1, 2, 3 ]"
    try:
      discard parseMessage(data)
      doAssert(false)
    except AssertionError:
      discard

  block:
    let data = "xxx"
    try:
      discard parseMessage(data)
      doAssert(false)
    except JsonParsingError:
      discard

  block:
    let msg = Message(user: "chris.sykes", message: "a message")
    doAssert(createMessage(msg).parseMessage() == msg)

  echo "Tests passed."
