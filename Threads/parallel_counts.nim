import os, parseutils, threadpool, strutils

type
  Stats = ref object
    domainCode, pageTitle: string
    viewCount, requestBytes: int


proc newStats(): Stats =
  Stats(domainCode: "", pageTitle: "", viewCount: 0, requestBytes: 0)


proc `$`(stats: Stats): string =
  "(domain: $#, title: $#, views: $#, bytes: $#)" % [
    stats.domainCode, stats.pageTitle, $stats.viewCount, $stats.requestBytes
  ]


proc parseLine(line: string,
               domainCode, pageTitle: var string,
               viewCount, requestBytes: var int) =
  if line.len == 0: return
  var i = 0
  domainCode.setLen(0)
  i.inc parseUntil(line, domainCode, {' '}, i)
  i.inc

  pageTitle.setLen(0)
  i.inc parseUntil(line, pageTitle, {' '}, i)
  i.inc

  viewCount = 0
  i.inc parseInt(line, viewCount, i)
  i.inc

  requestBytes = 0
  i.inc parseInt(line, requestBytes, i)


proc parseChunk(chunk: string): Stats =
  result = newStats()
  var domainCode = ""
  var pageTitle = ""
  var viewCount = 0
  var requestBytes = 0
  for line in splitLines(chunk):
    parseLine(line, domainCode, pageTitle, viewCount, requestBytes)
    if domainCode == "en" and viewCount > result.viewCount:
      result = Stats(domainCode: domainCode, pageTitle: pageTitle,
                     viewCount: viewCount, requestBytes: requestBytes)


proc readPageCounts(filename: string, chunkSize = 1_000_000): Stats =
  var file = open(filename)
  var responses = newSeq[FlowVar[Stats]]()
  var buffer = newString(chunkSize)
  var oldBufferLen = 0

  while not file.endOfFile:
    let reqSize = chunkSize - oldBufferLen
    assert reqSize > 0
    let readSize = file.readChars(buffer, oldBufferLen, reqSize) + oldBufferLen
    var chunkLen = readSize

    # Find the last newline in the chunk
    while chunkLen >= 0 and buffer[chunkLen - 1] notin NewLines:
      chunkLen.dec

    responses.add(spawn parseChunk(buffer[0 .. <chunkLen]))
    oldBufferLen = readSize - chunkLen
    buffer[0 .. <oldBufferLen] = buffer[readSize - oldBufferLen .. ^1]

  # Iterate the responses to find the most popular overall
  result = newStats()
  for resp in responses:
    let stats = ^resp
    if stats.viewCount > result.viewCount:
      result = stats

  file.close()
  

when isMainModule:
  const file = "pagecounts-20160101-050000"
  let filename = getCurrentDir() / file
  let top = readPageCounts(filename)
  echo("Top viewed English page: '", $top)

