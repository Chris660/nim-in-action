import os, parseutils

proc parseLine(line: string,
               domainCode, pageTitle: var string,
               viewCount, requestBytes: var int) =
  var i = 0
  domainCode.setLen(0)
  i.inc parseUntil(line, domainCode, {' '}, i)
  i.inc

  pageTitle.setLen(0)
  i.inc parseUntil(line, pageTitle, {' '}, i)
  i.inc

  viewCount = 0
  i.inc parseInt(line, viewCount, i)
  i.inc

  requestBytes = 0
  i.inc parseInt(line, requestBytes, i)

type PageStats = tuple[title: string, viewCount, requestBytes: int]

proc readPageCounts(filename: string) : PageStats =
  var domainCode = ""
  var pageTitle = ""
  var viewCount = 0
  var requestBytes = 0

  result.title = ""
  result.viewCount = 0
  result.requestBytes = 0

  for line in filename.lines:
    parseLine(line, domainCode, pageTitle, viewCount, requestBytes)
    if domainCode == "en" and viewCount > result.viewCount:
      result.title = pageTitle
      result.viewCount = viewCount
      result.requestBytes = requestBytes

when isMainModule:
  const file = "pagecounts-20160101-050000"
  let filename = getCurrentDir() / file
  let top = readPageCounts(filename)
  echo("Top viewed English page: '", top.title,
       "', views: ", $top.viewCount, ", bytes: ", $top.requestBytes)

