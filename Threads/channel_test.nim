import os, threadpool

var chan: Channel[string]
open(chan)

proc sayHello(to: string) =
  sleep(1000)
  chan.send("Hello " & to)

spawn sayHello("Chris")
let greeting = chan.recv()
echo greeting
