import re

let pattern = re"([^\s]+)\s([^\s]+)\s(\d+)\s(\d+)"

var line = "en Nim_(programming_language) 1 70231"
var matches: array[5, string]
let start = find(line, pattern, matches)
doAssert start == 0
echo(matches[0])
echo(matches[1])
echo(matches[3])
echo(matches[4])
echo("Parsed successfully!")

