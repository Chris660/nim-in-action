import threadpool, locks

var counterLock: Lock
initLock(counterLock)
var counter {.guard: counterLock.} = 0

proc increment(x: int) =
  for i in (0 .. x-1):
    withLock counterLock:
      counter.inc

spawn(increment(10_000))
spawn(increment(10_000))
sync()
echo(counter)

