# configurator
# Copyright Chris Sykes
# Configuration file parser

import json
import macros

proc createRefType(identifier: NimIdent,
                   definition: seq[NimNode]): NimNode =
  result = newTree(nnkTypeSection,
    newTree(nnkTypeDef,
      newIdentNode(identifier),
      newEmptyNode(),
      newTree(nnkRefTy,
        newTree(nnkObjectTy,
          newEmptyNode(),
          newEmptyNode(),
            newTree(nnkRecList,
              definition)))))

# Converts a "list" of call statements to a seq of `nnkIdentDefs` NimNodes
proc toIdentDefs(stmtList: NimNode): seq[NimNode] =
  expectKind(stmtList, nnkStmtList)
  result = @[]

  for node in stmtList:
    expectKind(node, nnkCall)
    result.add(
      newIdentDefs(
        node[0],     # The field name, such as Call -> Ident !"address".
        node[1][0])) # The field type, such as Call -> StmtList -> Ident !"string".


# Template to stamp out ref object ctors
template constructor(ident: untyped): untyped =
  proc `new ident`(): `ident` = # "new" and "ident" will be concatenated
    new result # create a new result, the type can be inferred.


# Generate a config file loader procedure
proc createLoadProc(typeName: NimIdent, identDefs: seq[NimNode]): NimNode =
  var cfgIdent = newIdentNode("cfg")
  var fileNameIdent = newIdentNode("fileName")
  var objIdent = newIdentNode("obj")
  var body = newStmtList()
  body.add quote do:
    var `objIdent` = parseFile(`fileNameIdent`)
    
  # now extract the JSON fields
  for identDef in identDefs:
    let fieldNameIdent = identDef[0]
    let fieldName = $fieldNameIdent.ident
    let fieldTypeName = $identDef[1].ident

    case fieldTypeName
      of "string":
        body.add quote do:
          `cfgIdent`.`fieldNameIdent` = `objIdent`[`fieldName`].getStr

      of "int":
        body.add quote do:
          `cfgIdent`.`fieldNameIdent` = `objIdent`[`fieldName`].getInt

      else:
        doAssert(false, "Unhandled type: " & fieldTypeName)

  return newProc(newIdentNode("load"),
    [newEmptyNode(), # proc return type: void
     newIdentDefs(cfgIdent, newIdentNode(typeName)), # cfg: typeName
     newIdentDefs(fileNameIdent, newIdentNode("string"))], body)
     

macro config(typeName: untyped, fields: untyped): untyped =
  result = newStmtList()
  let identDefs = toIdentDefs(fields)
  result.add createRefType(typeName.ident, identDefs)
  result.add getAST(constructor(typeName.ident))
  result.add createLoadProc(typeName.ident, identDefs)
  echo repr(result)


config MyAppConfig:
  address: string
  port: int

var myConf = newMyAppConfig()
myConf.load("MyAppConfig.json")
echo("Address: ", myConf.address)
echo("Port: ", myConf.port)

