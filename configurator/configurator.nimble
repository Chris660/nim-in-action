# Package

version       = "0.1.0"
author        = "Chris Sykes"
description   = "Configuration file parser"
license       = "MIT"
srcDir        = "src"

# Dependencies

requires "nim >= 0.18.0"
