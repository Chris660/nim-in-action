import macros

dumpTree:
  5 * (5 + 10)

macro calculate(): int =
  result = infix(
    newIntLitNode(5),
    "*",
    newPar(
      infix(
        newIntLitNode(5),
        "+",
        newIntLitNode(10)
      )
    )
  )

macro calculate(a, b, c: int): int =
  result = infix(
    `a`,
    "*",
    newPar(
      infix(
        `b`,
        "+",
        `c`
      )
    )
  )

echo calculate()
echo calculate(2, 3, 4)

