import os
import sdl

const INIT_VIDEO* = 0x00000020

if sdl.init(INIT_VIDEO) == -1:
  quit("Failed to initialise SDL!")

var window: SdlWindowPtr
var renderer: SdlRendererPtr

if createWindowAndRenderer(640, 480, 0, window, renderer) == -1:
  quit("Couldn't create a window or renderer")

# Ignore events, but call the EL to complete initialisation
# of window and renderer.
discard pollEvent(nil)

# Clear the buffer
renderer.setDrawColor 29, 64, 153, 255
renderer.clear

# Draw our "N"
renderer.setDrawColor 255, 255, 255, 255
var points = [
  (260'i32, 320'i32),
  (260'i32, 110'i32),
  (360'i32, 320'i32),
  (360'i32, 110'i32)
  ]

renderer.drawLines(addr points[0], points.len.cint)

renderer.present
sleep(5000)

